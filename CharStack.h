#define CHARSTACK
class CharStack {
private:
  CharStack *limitmax;
  char *stack;
  int maxSize;
  int size = 0;

public:
  CharStack(int max);
  ~CharStack();
  bool push(char c);
  char pop();
  bool isEmpty() const;
};