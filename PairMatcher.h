#define PAIRMATCHER
class PairMatcher {
private:
  char _openChar, _closeChar;
  CharStack charStack;

public:
  PairMatcher(char openChar, char closeChar);
  bool check(const string &testString);
};