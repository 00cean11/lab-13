#include "CharStack.h"
#include "PairMatcher.h"
#include <iostream>
#include <string>
using namespace std;

int main() {
  PairMatcher matcher('(', ')');

  string testString = "((())()";

  cout << testString << " is "
       << (matcher.check(testString) ? "valid" : "invalid") << endl;
  return 0;
}
