#include "PairMatcher.h"
PairMatcher::PairMatcher(char openChar, char closeChar) : charStack(100) {
  _openChar = openChar;
  _closeChar = closeChar;
}
bool PairMatcher::check(const string &testString) {
  for (int i = 0; i < testString.length(); i++) {
    if (testString[i] == _openChar) {
      charStack.push(_openChar);
    } else if (testString[i] == _closeChar) {
      if (charStack.pop() != _openChar) {
        return false;
      }
    }
  }
  return charStack.isEmpty();