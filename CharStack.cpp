#include "CharStack.h"
CharStack::CharStack(int max) {
  maxSize = max;
  stack = new char[maxSize];
}
CharStack::~CharStack() {
  delete[] stack;
  stack = nullptr;
}
bool CharStack::push(char c) {
  if (size == maxSize) {
    return false;
  }
  stack[size] = c;
  size++;
  return true;
}
char CharStack::pop() {
  if (isEmpty()) {
    return 0;
  }
  char retVal = stack[size - 1];
  size--;
  return retVal;
}
bool CharStack::isEmpty() const { return size == 0; }
